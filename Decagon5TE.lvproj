﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="12008004">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Dependencies" Type="Dependencies"/>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
	<Item Name="pervap-sheridan" Type="RT CompactRIO">
		<Property Name="alias.name" Type="Str">pervap-sheridan</Property>
		<Property Name="alias.value" Type="Str">10.210.211.220</Property>
		<Property Name="CCSymbols" Type="Str">TARGET_TYPE,RT;OS,VxWorks;CPU,PowerPC;</Property>
		<Property Name="crio.ControllerPID" Type="Str">73D2</Property>
		<Property Name="crio.family" Type="Str">901x</Property>
		<Property Name="host.ResponsivenessCheckEnabled" Type="Bool">true</Property>
		<Property Name="host.ResponsivenessCheckPingDelay" Type="UInt">5000</Property>
		<Property Name="host.ResponsivenessCheckPingTimeout" Type="UInt">1000</Property>
		<Property Name="host.TargetCPUID" Type="UInt">2</Property>
		<Property Name="host.TargetOSID" Type="UInt">14</Property>
		<Property Name="target.cleanupVisa" Type="Bool">false</Property>
		<Property Name="target.FPProtocolGlobals_ControlTimeLimit" Type="Int">300</Property>
		<Property Name="target.getDefault-&gt;WebServer.Port" Type="Int">80</Property>
		<Property Name="target.getDefault-&gt;WebServer.Timeout" Type="Int">60</Property>
		<Property Name="target.IOScan.Faults" Type="Str"></Property>
		<Property Name="target.IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="target.IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="target.IOScan.Period" Type="UInt">10000</Property>
		<Property Name="target.IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="target.IOScan.Priority" Type="UInt">0</Property>
		<Property Name="target.IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="target.IsRemotePanelSupported" Type="Bool">true</Property>
		<Property Name="target.RTCPULoadMonitoringEnabled" Type="Bool">true</Property>
		<Property Name="target.RTTarget.ApplicationPath" Type="Path">/c/ni-rt/startup/startup.rtexe</Property>
		<Property Name="target.RTTarget.EnableFileSharing" Type="Bool">true</Property>
		<Property Name="target.RTTarget.IPAccess" Type="Str">+*</Property>
		<Property Name="target.RTTarget.LaunchAppAtBoot" Type="Bool">false</Property>
		<Property Name="target.RTTarget.VIPath" Type="Path">/c/ni-rt/startup</Property>
		<Property Name="target.server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.server.tcp.access" Type="Str">+*</Property>
		<Property Name="target.server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="target.server.tcp.paranoid" Type="Bool">true</Property>
		<Property Name="target.server.tcp.port" Type="Int">3363</Property>
		<Property Name="target.server.tcp.serviceName" Type="Str">Main Application Instance/VI Server</Property>
		<Property Name="target.server.tcp.serviceName.default" Type="Str">Main Application Instance/VI Server</Property>
		<Property Name="target.server.vi.access" Type="Str">+*</Property>
		<Property Name="target.server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="target.server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.WebServer.Config" Type="Str">ServerName default
DocumentRoot "$LVSERVER_DOCROOT"
Listen 8000
ThreadLimit 10
TypesConfig "$LVSERVER_CONFIGROOT/mime.types"
DirectoryIndex index.htm
LoadModulePath $LVSERVER_MODULEPATHS
LoadModule LVAuth lvauthmodule
LoadModule LVRFP lvrfpmodule
LoadModule dir libdirModule
LoadModule copy libcopyModule

AddHandler LVAuthHandler
AddHandler LVRFPHandler

AddHandler dirHandler
AddHandler copyHandler 

KeepAlive on
KeepAliveTimeout 60
Timeout 60

</Property>
		<Property Name="target.WebServer.Enabled" Type="Bool">false</Property>
		<Property Name="target.WebServer.LogEnabled" Type="Bool">false</Property>
		<Property Name="target.WebServer.LogPath" Type="Path">/c/ni-rt/system/www/www.log</Property>
		<Property Name="target.WebServer.Port" Type="Int">80</Property>
		<Property Name="target.WebServer.RootPath" Type="Path">/c/ni-rt/system/www</Property>
		<Property Name="target.WebServer.TcpAccess" Type="Str">c+*</Property>
		<Property Name="target.WebServer.Timeout" Type="Int">60</Property>
		<Property Name="target.WebServer.ViAccess" Type="Str">+*</Property>
		<Property Name="target.webservices.SecurityAPIKey" Type="Str">PqVr/ifkAQh+lVrdPIykXlFvg12GhhQFR8H9cUhphgg=:pTe9HRlQuMfJxAG6QCGq7UvoUpJzAzWGKy5SbZ+roSU=</Property>
		<Property Name="target.webservices.ValidTimestampWindow" Type="Int">15</Property>
		<Item Name="subVI" Type="Folder" URL="../subVI">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="typedefs" Type="Folder" URL="../typedefs">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="Chassis" Type="cRIO Chassis">
			<Property Name="crio.ProgrammingMode" Type="Str">fpga</Property>
			<Property Name="crio.ResourceID" Type="Str">RIO0</Property>
			<Property Name="crio.Type" Type="Str">cRIO-9114</Property>
			<Item Name="FPGA Target" Type="FPGA Target">
				<Property Name="AutoRun" Type="Bool">false</Property>
				<Property Name="configString.guid" Type="Str">{01BB24D9-D0FE-4251-BED1-74937723F166}"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;FIFO uint32;DataType=1000800000000001000940070003553332000100000000000000000000;DisableOnOverflowUnderflow=FALSE"{03F53F69-C11C-4B73-BC97-0A75F1025C51}resource=/crio_Mod6/DIO15;0;ReadMethodType=bool;WriteMethodType=bool{05C56F73-49EB-4170-961B-F33B53BEA55F}resource=/Sleep;0;ReadMethodType=bool;WriteMethodType=bool{0914104A-EDEE-4BA9-95B7-A6DFBBA8E418}resource=/crio_Mod6/DIO25;0;ReadMethodType=bool;WriteMethodType=bool{0B497310-C9B3-47AD-A636-795C64209F44}resource=/crio_Mod5/DO10;0;ReadMethodType=bool;WriteMethodType=bool{128AE4C2-52C3-4F08-BAFD-CBF53EE925E0}resource=/crio_Mod6/DIO19;0;ReadMethodType=bool;WriteMethodType=bool{1A2DBD0E-7190-4CB2-A880-57B7A5689773}resource=/crio_Mod6/DIO27;0;ReadMethodType=bool;WriteMethodType=bool{1B56A331-12FC-45A0-A4E3-37C51C061463}resource=/crio_Mod5/DO17;0;ReadMethodType=bool;WriteMethodType=bool{22EAC336-D2C4-4D6F-AF20-E437DB291DF4}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 4,crio.Type=NI 9208,cRIOModule.AI0.DegreeRange=2,cRIOModule.AI0.TCoupleType=0,cRIOModule.AI1.DegreeRange=2,cRIOModule.AI1.TCoupleType=0,cRIOModule.AI10.DegreeRange=2,cRIOModule.AI10.TCoupleType=0,cRIOModule.AI11.DegreeRange=2,cRIOModule.AI11.TCoupleType=0,cRIOModule.AI12.DegreeRange=2,cRIOModule.AI12.TCoupleType=0,cRIOModule.AI13.DegreeRange=2,cRIOModule.AI13.TCoupleType=0,cRIOModule.AI14.DegreeRange=2,cRIOModule.AI14.TCoupleType=0,cRIOModule.AI15.DegreeRange=2,cRIOModule.AI15.TCoupleType=0,cRIOModule.AI2.DegreeRange=2,cRIOModule.AI2.TCoupleType=0,cRIOModule.AI3.DegreeRange=2,cRIOModule.AI3.TCoupleType=0,cRIOModule.AI4.DegreeRange=2,cRIOModule.AI4.TCoupleType=0,cRIOModule.AI5.DegreeRange=2,cRIOModule.AI5.TCoupleType=0,cRIOModule.AI6.DegreeRange=2,cRIOModule.AI6.TCoupleType=0,cRIOModule.AI7.DegreeRange=2,cRIOModule.AI7.TCoupleType=0,cRIOModule.AI8.DegreeRange=2,cRIOModule.AI8.TCoupleType=0,cRIOModule.AI9.DegreeRange=2,cRIOModule.AI9.TCoupleType=0,cRIOModule.Conversion Time=1,cRIOModule.Enable Open TC Detection=true,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{272078C6-9FC1-43E8-A5E9-92D79749CA53}resource=/crio_Mod5/DO15;0;ReadMethodType=bool;WriteMethodType=bool{2E6B9F3F-CEB7-4B1C-B1DC-E8BBDEBC6042}resource=/crio_Mod6/DIO7:0;0;ReadMethodType=u8;WriteMethodType=u8{2F102F6E-FDBB-40C9-8193-0BDF1D0506C7}resource=/System Reset;0;ReadMethodType=bool;WriteMethodType=bool{301132FA-CD42-4E23-BDBA-6E8B3BC49536}resource=/crio_Mod6/DIO15:8;0;ReadMethodType=u8;WriteMethodType=u8{331C7A53-9B72-4026-90B1-F621E3838009}resource=/crio_Mod5/DO31:0;0;ReadMethodType=u32;WriteMethodType=u32{35710EAD-C83B-419E-B1C2-7D141CC1D644}resource=/crio_Mod6/DIO14;0;ReadMethodType=bool;WriteMethodType=bool{35F8CE9F-BC5A-48AE-BAD2-52DE823D132D}resource=/crio_Mod6/DIO0;0;ReadMethodType=bool;WriteMethodType=bool{37D42333-D014-4484-B80B-8D29113D9B0E}resource=/crio_Mod6/DIO2;0;ReadMethodType=bool;WriteMethodType=bool{3D42045F-DE61-4EC5-9446-1302668E183D}resource=/FPGA LED;0;ReadMethodType=bool;WriteMethodType=bool{3DF8D566-6C1C-4B89-8BEB-1303BB1500CC}resource=/crio_Mod5/DO2;0;ReadMethodType=bool;WriteMethodType=bool{3FEDF4DC-2E36-450E-966D-70F56CACA015}resource=/crio_Mod6/DIO20;0;ReadMethodType=bool;WriteMethodType=bool{468A3CF1-2699-4647-8656-B5D9AB6FA27F}resource=/crio_Mod5/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8{502211C3-860E-40C3-8F96-146BAEB9042E}resource=/crio_Mod6/DIO1;0;ReadMethodType=bool;WriteMethodType=bool{53B1F9CE-A6E0-44AC-8D10-D49700B41049}resource=/Chassis Temperature;0;ReadMethodType=i16{5CEDF3E5-DC62-4E1F-BC1B-0C0AD13232AF}resource=/crio_Mod5/DO23;0;ReadMethodType=bool;WriteMethodType=bool{60A18288-CA9D-4EDE-8CA4-C0AEE259073A}resource=/crio_Mod5/DO18;0;ReadMethodType=bool;WriteMethodType=bool{6132DB0A-F9DB-4CDE-A1C4-23B42C4C4DE1}resource=/crio_Mod6/DIO22;0;ReadMethodType=bool;WriteMethodType=bool{61A861C0-FE35-45A0-8751-5582C05B485C}resource=/crio_Mod6/DIO11;0;ReadMethodType=bool;WriteMethodType=bool{6318C4D7-C920-418D-A0DF-435ED4F3E5E7}resource=/crio_Mod6/DIO4;0;ReadMethodType=bool;WriteMethodType=bool{6350A5C2-EEC0-44E8-A28C-BE72EBA13269}resource=/crio_Mod5/DO25;0;ReadMethodType=bool;WriteMethodType=bool{643C0700-2B47-45D7-B5D9-E141C5ED49C3}resource=/crio_Mod5/DO13;0;ReadMethodType=bool;WriteMethodType=bool{6442482D-7FB8-4E76-801F-DEFC530F4AC6}resource=/crio_Mod5/DO9;0;ReadMethodType=bool;WriteMethodType=bool{64F64DAE-2B98-442D-936F-0E7CC082A87F}resource=/crio_Mod6/DIO24;0;ReadMethodType=bool;WriteMethodType=bool{66103C24-4578-4E49-A423-872E667BA772}resource=/crio_Mod5/DO28;0;ReadMethodType=bool;WriteMethodType=bool{6627E99C-CF72-47E7-A944-20965D2B6EA3}resource=/crio_Mod6/DIO21;0;ReadMethodType=bool;WriteMethodType=bool{6F0C8364-A4C5-42E4-B2E0-27CD1A170F11}resource=/crio_Mod5/DO16;0;ReadMethodType=bool;WriteMethodType=bool{6FFCD3EF-128E-4462-B6BE-38A9540A307B}resource=/crio_Mod5/DO7;0;ReadMethodType=bool;WriteMethodType=bool{71D61863-399E-4C5E-9403-9C693F904EF3}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 5,crio.Type=NI 9476,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{7565E598-31EF-40D2-86EB-16CEEE2B3E8D}resource=/crio_Mod6/DIO12;0;ReadMethodType=bool;WriteMethodType=bool{79A6C91A-26D2-433D-AD65-700717963D8B}resource=/crio_Mod5/DO24;0;ReadMethodType=bool;WriteMethodType=bool{7FE88EC3-E7C5-457D-8D6A-88AAE45409F7}resource=/crio_Mod5/DO22;0;ReadMethodType=bool;WriteMethodType=bool{80B0EFCF-27E8-480A-8E8D-0232BB149B89}resource=/crio_Mod5/DO4;0;ReadMethodType=bool;WriteMethodType=bool{84E95502-F26E-4203-A62A-ED8D9215DD2A}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E{8F5A8316-30FB-4C68-8066-EB96FA55C477}resource=/crio_Mod5/DO27;0;ReadMethodType=bool;WriteMethodType=bool{93B597CC-B72C-475B-92D7-340663CF9370}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 6,crio.Type=NI 9403,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.Initial Line Direction=00000000000000000000000000000000,cRIOModule.RsiAttributes=[crioConfig.End]{93D2B685-7A60-4DD8-944F-43C66564081B}resource=/crio_Mod6/DIO26;0;ReadMethodType=bool;WriteMethodType=bool{9704869F-B668-4C38-9A20-26B00FFEDCE8}resource=/crio_Mod6/DIO28;0;ReadMethodType=bool;WriteMethodType=bool{9BDFED24-98A4-404A-9769-4EF16E7DA16A}resource=/crio_Mod5/DO6;0;ReadMethodType=bool;WriteMethodType=bool{9CCD7F23-6CFE-45A1-BE35-53F7B960D513}resource=/crio_Mod6/DIO13;0;ReadMethodType=bool;WriteMethodType=bool{A3954AAF-E743-4A06-B53B-022B82C35B89}resource=/crio_Mod5/DO8;0;ReadMethodType=bool;WriteMethodType=bool{A8392325-BEAF-4006-807D-4FB31F8C8227}resource=/crio_Mod6/DIO16;0;ReadMethodType=bool;WriteMethodType=bool{AA9BADF8-7F65-4C1D-9B87-1046E4528118}resource=/crio_Mod6/DIO5;0;ReadMethodType=bool;WriteMethodType=bool{AC14051D-D682-4466-A09F-3A34527FE7CC}resource=/crio_Mod5/DO31;0;ReadMethodType=bool;WriteMethodType=bool{ACF6E28A-50A9-4C7E-8B80-6478B56F3F52}resource=/crio_Mod6/DIO23:16;0;ReadMethodType=u8;WriteMethodType=u8{AE4095E2-F135-4177-AD13-D82352987CDD}resource=/crio_Mod6/DIO17;0;ReadMethodType=bool;WriteMethodType=bool{AE775159-FE81-4661-8C43-3EF2DFE94A31}resource=/crio_Mod6/DIO31;0;ReadMethodType=bool;WriteMethodType=bool{B0BF2D7A-5BE1-49E6-9B61-7C4052BF04CA}resource=/crio_Mod6/DIO31:0;0;ReadMethodType=u32;WriteMethodType=u32{B3EA85CF-673D-4FAA-958B-5A04A66294F1}resource=/crio_Mod5/DO19;0;ReadMethodType=bool;WriteMethodType=bool{B4117E37-6C92-44BE-9464-D86B2772B036}resource=/crio_Mod5/DO0;0;ReadMethodType=bool;WriteMethodType=bool{B9FFE86E-B964-40E5-8449-3B4AA1370004}resource=/crio_Mod5/DO20;0;ReadMethodType=bool;WriteMethodType=bool{BB589436-01B0-4A3C-B988-E80250321291}resource=/crio_Mod5/DO30;0;ReadMethodType=bool;WriteMethodType=bool{BB68FF90-E203-4D9C-8308-32DEDBF37243}resource=/crio_Mod5/DO21;0;ReadMethodType=bool;WriteMethodType=bool{BCC3F639-C4D2-42CB-AE8A-6F223391835B}resource=/crio_Mod6/DIO9;0;ReadMethodType=bool;WriteMethodType=bool{C3F2F0E3-8395-4CFB-9CAF-0FD42E696B54}resource=/crio_Mod5/DO29;0;ReadMethodType=bool;WriteMethodType=bool{C85EC4A9-8773-4357-85DF-C24E00F1B316}resource=/crio_Mod6/DIO7;0;ReadMethodType=bool;WriteMethodType=bool{C9EC5D32-2464-448F-8B83-575A7AB4FF88}resource=/crio_Mod6/DIO18;0;ReadMethodType=bool;WriteMethodType=bool{CDA2A2EC-ECCB-4DE8-BC35-71F28BD9476E}resource=/crio_Mod5/DO26;0;ReadMethodType=bool;WriteMethodType=bool{D3EFAAE5-B4B5-492E-9270-07B3F0929A68}resource=/crio_Mod5/DO31:24;0;ReadMethodType=u8;WriteMethodType=u8{D59F6A5E-350F-4D4B-B4E6-A1BA7F96DD3D}resource=/crio_Mod5/DO11;0;ReadMethodType=bool;WriteMethodType=bool{E0078FE3-2AA5-4BDA-8099-5ABCE365B6BD}resource=/crio_Mod5/DO15:8;0;ReadMethodType=u8;WriteMethodType=u8{E21EC03B-B3EB-4EB7-A8C4-DC86157F62BB}resource=/crio_Mod6/DIO6;0;ReadMethodType=bool;WriteMethodType=bool{E38FC26E-8DCE-4356-9C20-578F637AFCBB}resource=/crio_Mod6/DIO23;0;ReadMethodType=bool;WriteMethodType=bool{E4C3FD36-96B3-4C9F-9FD2-DBB47F0EEE2E}resource=/crio_Mod6/DIO29;0;ReadMethodType=bool;WriteMethodType=bool{E8AE03B9-C593-4495-AFA9-EEBCF5DDC280}resource=/crio_Mod6/DIO31:24;0;ReadMethodType=u8;WriteMethodType=u8{E8CD583D-CCEE-438A-B652-D7D3A6D1C2B9}resource=/crio_Mod6/DIO8;0;ReadMethodType=bool;WriteMethodType=bool{ECB3CA64-FD9E-41AB-8FBB-7056C486DE98}resource=/crio_Mod5/DO5;0;ReadMethodType=bool;WriteMethodType=bool{ED9A4AD4-93CA-48AB-BF36-8B208B619591}resource=/crio_Mod5/DO3;0;ReadMethodType=bool;WriteMethodType=bool{F1CF0ED8-4498-48F4-AC96-AEA85FFF7926}resource=/crio_Mod6/DIO10;0;ReadMethodType=bool;WriteMethodType=bool{F63D493A-3DF9-4FA5-BFD1-1778D7E39CBE}resource=/Scan Clock;0;ReadMethodType=bool{FAA7D784-8311-4173-9C01-32D4D0A0330E}resource=/crio_Mod5/DO1;0;ReadMethodType=bool;WriteMethodType=bool{FB614C91-BCFD-4D4E-90D0-30446CD8993B}resource=/crio_Mod6/DIO30;0;ReadMethodType=bool;WriteMethodType=bool{FBAAF3F9-C7BD-400B-B0BB-FE72E1000E54}resource=/crio_Mod5/DO14;0;ReadMethodType=bool;WriteMethodType=bool{FE9611FE-9F31-4B0B-A2E7-B675AC3530B7}resource=/crio_Mod6/DIO3;0;ReadMethodType=bool;WriteMethodType=bool{FEBA6F38-0FC7-4EF7-8694-39BFAA2581E7}resource=/crio_Mod5/DO23:16;0;ReadMethodType=u8;WriteMethodType=u8{FF90BB4B-FC8A-4B4C-920E-C3F83656BA8A}resource=/crio_Mod5/DO12;0;ReadMethodType=bool;WriteMethodType=boolcRIO-9114/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA</Property>
				<Property Name="configString.name" Type="Str">40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427EChassis Temperatureresource=/Chassis Temperature;0;ReadMethodType=i16cRIO-9114/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGAFIFO uint32"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;FIFO uint32;DataType=1000800000000001000940070003553332000100000000000000000000;DisableOnOverflowUnderflow=FALSE"FPGA LEDresource=/FPGA LED;0;ReadMethodType=bool;WriteMethodType=boolMod4[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 4,crio.Type=NI 9208,cRIOModule.AI0.DegreeRange=2,cRIOModule.AI0.TCoupleType=0,cRIOModule.AI1.DegreeRange=2,cRIOModule.AI1.TCoupleType=0,cRIOModule.AI10.DegreeRange=2,cRIOModule.AI10.TCoupleType=0,cRIOModule.AI11.DegreeRange=2,cRIOModule.AI11.TCoupleType=0,cRIOModule.AI12.DegreeRange=2,cRIOModule.AI12.TCoupleType=0,cRIOModule.AI13.DegreeRange=2,cRIOModule.AI13.TCoupleType=0,cRIOModule.AI14.DegreeRange=2,cRIOModule.AI14.TCoupleType=0,cRIOModule.AI15.DegreeRange=2,cRIOModule.AI15.TCoupleType=0,cRIOModule.AI2.DegreeRange=2,cRIOModule.AI2.TCoupleType=0,cRIOModule.AI3.DegreeRange=2,cRIOModule.AI3.TCoupleType=0,cRIOModule.AI4.DegreeRange=2,cRIOModule.AI4.TCoupleType=0,cRIOModule.AI5.DegreeRange=2,cRIOModule.AI5.TCoupleType=0,cRIOModule.AI6.DegreeRange=2,cRIOModule.AI6.TCoupleType=0,cRIOModule.AI7.DegreeRange=2,cRIOModule.AI7.TCoupleType=0,cRIOModule.AI8.DegreeRange=2,cRIOModule.AI8.TCoupleType=0,cRIOModule.AI9.DegreeRange=2,cRIOModule.AI9.TCoupleType=0,cRIOModule.Conversion Time=1,cRIOModule.Enable Open TC Detection=true,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Mod5/DO0resource=/crio_Mod5/DO0;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO10resource=/crio_Mod5/DO10;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO11resource=/crio_Mod5/DO11;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO12resource=/crio_Mod5/DO12;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO13resource=/crio_Mod5/DO13;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO14resource=/crio_Mod5/DO14;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO15:8resource=/crio_Mod5/DO15:8;0;ReadMethodType=u8;WriteMethodType=u8Mod5/DO15resource=/crio_Mod5/DO15;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO16resource=/crio_Mod5/DO16;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO17resource=/crio_Mod5/DO17;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO18resource=/crio_Mod5/DO18;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO19resource=/crio_Mod5/DO19;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO1resource=/crio_Mod5/DO1;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO20resource=/crio_Mod5/DO20;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO21resource=/crio_Mod5/DO21;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO22resource=/crio_Mod5/DO22;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO23:16resource=/crio_Mod5/DO23:16;0;ReadMethodType=u8;WriteMethodType=u8Mod5/DO23resource=/crio_Mod5/DO23;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO24resource=/crio_Mod5/DO24;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO25resource=/crio_Mod5/DO25;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO26resource=/crio_Mod5/DO26;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO27resource=/crio_Mod5/DO27;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO28resource=/crio_Mod5/DO28;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO29resource=/crio_Mod5/DO29;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO2resource=/crio_Mod5/DO2;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO30resource=/crio_Mod5/DO30;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO31:0resource=/crio_Mod5/DO31:0;0;ReadMethodType=u32;WriteMethodType=u32Mod5/DO31:24resource=/crio_Mod5/DO31:24;0;ReadMethodType=u8;WriteMethodType=u8Mod5/DO31resource=/crio_Mod5/DO31;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO3resource=/crio_Mod5/DO3;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO4resource=/crio_Mod5/DO4;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO5resource=/crio_Mod5/DO5;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO6resource=/crio_Mod5/DO6;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO7:0resource=/crio_Mod5/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8Mod5/DO7resource=/crio_Mod5/DO7;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO8resource=/crio_Mod5/DO8;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO9resource=/crio_Mod5/DO9;0;ReadMethodType=bool;WriteMethodType=boolMod5[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 5,crio.Type=NI 9476,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Mod6/DIO0resource=/crio_Mod6/DIO0;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO10resource=/crio_Mod6/DIO10;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO11resource=/crio_Mod6/DIO11;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO12resource=/crio_Mod6/DIO12;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO13resource=/crio_Mod6/DIO13;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO14resource=/crio_Mod6/DIO14;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO15:8resource=/crio_Mod6/DIO15:8;0;ReadMethodType=u8;WriteMethodType=u8Mod6/DIO15resource=/crio_Mod6/DIO15;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO16resource=/crio_Mod6/DIO16;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO17resource=/crio_Mod6/DIO17;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO18resource=/crio_Mod6/DIO18;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO19resource=/crio_Mod6/DIO19;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO1resource=/crio_Mod6/DIO1;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO20resource=/crio_Mod6/DIO20;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO21resource=/crio_Mod6/DIO21;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO22resource=/crio_Mod6/DIO22;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO23:16resource=/crio_Mod6/DIO23:16;0;ReadMethodType=u8;WriteMethodType=u8Mod6/DIO23resource=/crio_Mod6/DIO23;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO24resource=/crio_Mod6/DIO24;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO25resource=/crio_Mod6/DIO25;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO26resource=/crio_Mod6/DIO26;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO27resource=/crio_Mod6/DIO27;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO28resource=/crio_Mod6/DIO28;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO29resource=/crio_Mod6/DIO29;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO2resource=/crio_Mod6/DIO2;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO30resource=/crio_Mod6/DIO30;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO31:0resource=/crio_Mod6/DIO31:0;0;ReadMethodType=u32;WriteMethodType=u32Mod6/DIO31:24resource=/crio_Mod6/DIO31:24;0;ReadMethodType=u8;WriteMethodType=u8Mod6/DIO31resource=/crio_Mod6/DIO31;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO3resource=/crio_Mod6/DIO3;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO4resource=/crio_Mod6/DIO4;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO5resource=/crio_Mod6/DIO5;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO6resource=/crio_Mod6/DIO6;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO7:0resource=/crio_Mod6/DIO7:0;0;ReadMethodType=u8;WriteMethodType=u8Mod6/DIO7resource=/crio_Mod6/DIO7;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO8resource=/crio_Mod6/DIO8;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO9resource=/crio_Mod6/DIO9;0;ReadMethodType=bool;WriteMethodType=boolMod6[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 6,crio.Type=NI 9403,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.Initial Line Direction=00000000000000000000000000000000,cRIOModule.RsiAttributes=[crioConfig.End]Scan Clockresource=/Scan Clock;0;ReadMethodType=boolSleepresource=/Sleep;0;ReadMethodType=bool;WriteMethodType=boolSystem Resetresource=/System Reset;0;ReadMethodType=bool;WriteMethodType=bool</Property>
				<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">cRIO-9114/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA</Property>
				<Property Name="NI.LV.FPGA.Version" Type="Int">5</Property>
				<Property Name="NI.SortType" Type="Int">3</Property>
				<Property Name="Resource Name" Type="Str">RIO0</Property>
				<Property Name="Target Class" Type="Str">cRIO-9114</Property>
				<Property Name="Top-Level Timing Source" Type="Str">40 MHz Onboard Clock</Property>
				<Property Name="Top-Level Timing Source Is Default" Type="Bool">true</Property>
				<Item Name="Chassis I/O" Type="Folder">
					<Item Name="Chassis Temperature" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/Chassis Temperature</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{53B1F9CE-A6E0-44AC-8D10-D49700B41049}</Property>
					</Item>
					<Item Name="FPGA LED" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/FPGA LED</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{3D42045F-DE61-4EC5-9446-1302668E183D}</Property>
					</Item>
					<Item Name="Scan Clock" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/Scan Clock</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{F63D493A-3DF9-4FA5-BFD1-1778D7E39CBE}</Property>
					</Item>
					<Item Name="Sleep" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/Sleep</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{05C56F73-49EB-4170-961B-F33B53BEA55F}</Property>
					</Item>
					<Item Name="System Reset" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/System Reset</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{2F102F6E-FDBB-40C9-8193-0BDF1D0506C7}</Property>
					</Item>
				</Item>
				<Item Name="digitalOut" Type="Folder">
					<Item Name="Mod5/DO0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod5/DO0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{B4117E37-6C92-44BE-9464-D86B2772B036}</Property>
					</Item>
					<Item Name="Mod5/DO1" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod5/DO1</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{FAA7D784-8311-4173-9C01-32D4D0A0330E}</Property>
					</Item>
					<Item Name="Mod5/DO2" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod5/DO2</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{3DF8D566-6C1C-4B89-8BEB-1303BB1500CC}</Property>
					</Item>
					<Item Name="Mod5/DO3" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod5/DO3</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{ED9A4AD4-93CA-48AB-BF36-8B208B619591}</Property>
					</Item>
					<Item Name="Mod5/DO4" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod5/DO4</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{80B0EFCF-27E8-480A-8E8D-0232BB149B89}</Property>
					</Item>
					<Item Name="Mod5/DO5" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod5/DO5</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{ECB3CA64-FD9E-41AB-8FBB-7056C486DE98}</Property>
					</Item>
					<Item Name="Mod5/DO6" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod5/DO6</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{9BDFED24-98A4-404A-9769-4EF16E7DA16A}</Property>
					</Item>
					<Item Name="Mod5/DO7" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod5/DO7</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{6FFCD3EF-128E-4462-B6BE-38A9540A307B}</Property>
					</Item>
					<Item Name="Mod5/DO7:0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod5/DO7:0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{468A3CF1-2699-4647-8656-B5D9AB6FA27F}</Property>
					</Item>
					<Item Name="Mod5/DO8" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod5/DO8</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{A3954AAF-E743-4A06-B53B-022B82C35B89}</Property>
					</Item>
					<Item Name="Mod5/DO9" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod5/DO9</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{6442482D-7FB8-4E76-801F-DEFC530F4AC6}</Property>
					</Item>
					<Item Name="Mod5/DO10" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod5/DO10</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{0B497310-C9B3-47AD-A636-795C64209F44}</Property>
					</Item>
					<Item Name="Mod5/DO11" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod5/DO11</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{D59F6A5E-350F-4D4B-B4E6-A1BA7F96DD3D}</Property>
					</Item>
					<Item Name="Mod5/DO12" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod5/DO12</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{FF90BB4B-FC8A-4B4C-920E-C3F83656BA8A}</Property>
					</Item>
					<Item Name="Mod5/DO13" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod5/DO13</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{643C0700-2B47-45D7-B5D9-E141C5ED49C3}</Property>
					</Item>
					<Item Name="Mod5/DO14" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod5/DO14</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{FBAAF3F9-C7BD-400B-B0BB-FE72E1000E54}</Property>
					</Item>
					<Item Name="Mod5/DO15" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod5/DO15</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{272078C6-9FC1-43E8-A5E9-92D79749CA53}</Property>
					</Item>
					<Item Name="Mod5/DO15:8" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod5/DO15:8</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{E0078FE3-2AA5-4BDA-8099-5ABCE365B6BD}</Property>
					</Item>
					<Item Name="Mod5/DO16" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod5/DO16</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{6F0C8364-A4C5-42E4-B2E0-27CD1A170F11}</Property>
					</Item>
					<Item Name="Mod5/DO17" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod5/DO17</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{1B56A331-12FC-45A0-A4E3-37C51C061463}</Property>
					</Item>
					<Item Name="Mod5/DO18" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod5/DO18</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{60A18288-CA9D-4EDE-8CA4-C0AEE259073A}</Property>
					</Item>
					<Item Name="Mod5/DO19" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod5/DO19</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{B3EA85CF-673D-4FAA-958B-5A04A66294F1}</Property>
					</Item>
					<Item Name="Mod5/DO20" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod5/DO20</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{B9FFE86E-B964-40E5-8449-3B4AA1370004}</Property>
					</Item>
					<Item Name="Mod5/DO21" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod5/DO21</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{BB68FF90-E203-4D9C-8308-32DEDBF37243}</Property>
					</Item>
					<Item Name="Mod5/DO22" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod5/DO22</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{7FE88EC3-E7C5-457D-8D6A-88AAE45409F7}</Property>
					</Item>
					<Item Name="Mod5/DO23" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod5/DO23</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{5CEDF3E5-DC62-4E1F-BC1B-0C0AD13232AF}</Property>
					</Item>
					<Item Name="Mod5/DO23:16" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod5/DO23:16</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{FEBA6F38-0FC7-4EF7-8694-39BFAA2581E7}</Property>
					</Item>
					<Item Name="Mod5/DO24" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod5/DO24</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{79A6C91A-26D2-433D-AD65-700717963D8B}</Property>
					</Item>
					<Item Name="Mod5/DO25" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod5/DO25</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{6350A5C2-EEC0-44E8-A28C-BE72EBA13269}</Property>
					</Item>
					<Item Name="Mod5/DO26" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod5/DO26</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{CDA2A2EC-ECCB-4DE8-BC35-71F28BD9476E}</Property>
					</Item>
					<Item Name="Mod5/DO27" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod5/DO27</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{8F5A8316-30FB-4C68-8066-EB96FA55C477}</Property>
					</Item>
					<Item Name="Mod5/DO28" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod5/DO28</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{66103C24-4578-4E49-A423-872E667BA772}</Property>
					</Item>
					<Item Name="Mod5/DO29" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod5/DO29</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{C3F2F0E3-8395-4CFB-9CAF-0FD42E696B54}</Property>
					</Item>
					<Item Name="Mod5/DO30" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod5/DO30</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{BB589436-01B0-4A3C-B988-E80250321291}</Property>
					</Item>
					<Item Name="Mod5/DO31" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod5/DO31</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{AC14051D-D682-4466-A09F-3A34527FE7CC}</Property>
					</Item>
					<Item Name="Mod5/DO31:0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod5/DO31:0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{331C7A53-9B72-4026-90B1-F621E3838009}</Property>
					</Item>
					<Item Name="Mod5/DO31:24" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod5/DO31:24</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{D3EFAAE5-B4B5-492E-9270-07B3F0929A68}</Property>
					</Item>
				</Item>
				<Item Name="digitalIO" Type="Folder">
					<Item Name="Mod6/DIO0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod6/DIO0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{35F8CE9F-BC5A-48AE-BAD2-52DE823D132D}</Property>
					</Item>
					<Item Name="Mod6/DIO1" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod6/DIO1</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{502211C3-860E-40C3-8F96-146BAEB9042E}</Property>
					</Item>
					<Item Name="Mod6/DIO2" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod6/DIO2</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{37D42333-D014-4484-B80B-8D29113D9B0E}</Property>
					</Item>
					<Item Name="Mod6/DIO3" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod6/DIO3</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{FE9611FE-9F31-4B0B-A2E7-B675AC3530B7}</Property>
					</Item>
					<Item Name="Mod6/DIO4" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod6/DIO4</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{6318C4D7-C920-418D-A0DF-435ED4F3E5E7}</Property>
					</Item>
					<Item Name="Mod6/DIO5" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod6/DIO5</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{AA9BADF8-7F65-4C1D-9B87-1046E4528118}</Property>
					</Item>
					<Item Name="Mod6/DIO6" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod6/DIO6</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{E21EC03B-B3EB-4EB7-A8C4-DC86157F62BB}</Property>
					</Item>
					<Item Name="Mod6/DIO7" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod6/DIO7</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{C85EC4A9-8773-4357-85DF-C24E00F1B316}</Property>
					</Item>
					<Item Name="Mod6/DIO7:0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod6/DIO7:0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{2E6B9F3F-CEB7-4B1C-B1DC-E8BBDEBC6042}</Property>
					</Item>
					<Item Name="Mod6/DIO8" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod6/DIO8</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{E8CD583D-CCEE-438A-B652-D7D3A6D1C2B9}</Property>
					</Item>
					<Item Name="Mod6/DIO9" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod6/DIO9</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{BCC3F639-C4D2-42CB-AE8A-6F223391835B}</Property>
					</Item>
					<Item Name="Mod6/DIO10" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod6/DIO10</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{F1CF0ED8-4498-48F4-AC96-AEA85FFF7926}</Property>
					</Item>
					<Item Name="Mod6/DIO11" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod6/DIO11</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{61A861C0-FE35-45A0-8751-5582C05B485C}</Property>
					</Item>
					<Item Name="Mod6/DIO12" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod6/DIO12</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{7565E598-31EF-40D2-86EB-16CEEE2B3E8D}</Property>
					</Item>
					<Item Name="Mod6/DIO13" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod6/DIO13</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{9CCD7F23-6CFE-45A1-BE35-53F7B960D513}</Property>
					</Item>
					<Item Name="Mod6/DIO14" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod6/DIO14</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{35710EAD-C83B-419E-B1C2-7D141CC1D644}</Property>
					</Item>
					<Item Name="Mod6/DIO15" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod6/DIO15</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{03F53F69-C11C-4B73-BC97-0A75F1025C51}</Property>
					</Item>
					<Item Name="Mod6/DIO15:8" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod6/DIO15:8</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{301132FA-CD42-4E23-BDBA-6E8B3BC49536}</Property>
					</Item>
					<Item Name="Mod6/DIO16" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod6/DIO16</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{A8392325-BEAF-4006-807D-4FB31F8C8227}</Property>
					</Item>
					<Item Name="Mod6/DIO17" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod6/DIO17</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{AE4095E2-F135-4177-AD13-D82352987CDD}</Property>
					</Item>
					<Item Name="Mod6/DIO18" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod6/DIO18</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{C9EC5D32-2464-448F-8B83-575A7AB4FF88}</Property>
					</Item>
					<Item Name="Mod6/DIO19" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod6/DIO19</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{128AE4C2-52C3-4F08-BAFD-CBF53EE925E0}</Property>
					</Item>
					<Item Name="Mod6/DIO20" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod6/DIO20</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{3FEDF4DC-2E36-450E-966D-70F56CACA015}</Property>
					</Item>
					<Item Name="Mod6/DIO21" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod6/DIO21</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{6627E99C-CF72-47E7-A944-20965D2B6EA3}</Property>
					</Item>
					<Item Name="Mod6/DIO22" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod6/DIO22</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{6132DB0A-F9DB-4CDE-A1C4-23B42C4C4DE1}</Property>
					</Item>
					<Item Name="Mod6/DIO23" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod6/DIO23</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{E38FC26E-8DCE-4356-9C20-578F637AFCBB}</Property>
					</Item>
					<Item Name="Mod6/DIO23:16" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod6/DIO23:16</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{ACF6E28A-50A9-4C7E-8B80-6478B56F3F52}</Property>
					</Item>
					<Item Name="Mod6/DIO24" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod6/DIO24</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{64F64DAE-2B98-442D-936F-0E7CC082A87F}</Property>
					</Item>
					<Item Name="Mod6/DIO25" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod6/DIO25</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{0914104A-EDEE-4BA9-95B7-A6DFBBA8E418}</Property>
					</Item>
					<Item Name="Mod6/DIO26" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod6/DIO26</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{93D2B685-7A60-4DD8-944F-43C66564081B}</Property>
					</Item>
					<Item Name="Mod6/DIO27" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod6/DIO27</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{1A2DBD0E-7190-4CB2-A880-57B7A5689773}</Property>
					</Item>
					<Item Name="Mod6/DIO28" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod6/DIO28</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{9704869F-B668-4C38-9A20-26B00FFEDCE8}</Property>
					</Item>
					<Item Name="Mod6/DIO29" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod6/DIO29</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{E4C3FD36-96B3-4C9F-9FD2-DBB47F0EEE2E}</Property>
					</Item>
					<Item Name="Mod6/DIO30" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod6/DIO30</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{FB614C91-BCFD-4D4E-90D0-30446CD8993B}</Property>
					</Item>
					<Item Name="Mod6/DIO31" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod6/DIO31</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{AE775159-FE81-4661-8C43-3EF2DFE94A31}</Property>
					</Item>
					<Item Name="Mod6/DIO31:0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod6/DIO31:0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{B0BF2D7A-5BE1-49E6-9B61-7C4052BF04CA}</Property>
					</Item>
					<Item Name="Mod6/DIO31:24" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod6/DIO31:24</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{E8AE03B9-C593-4495-AFA9-EEBCF5DDC280}</Property>
					</Item>
				</Item>
				<Item Name="40 MHz Onboard Clock" Type="FPGA Base Clock">
					<Property Name="FPGA.PersistentID" Type="Str">{84E95502-F26E-4203-A62A-ED8D9215DD2A}</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig" Type="Str">ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.Accuracy" Type="Dbl">100</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.ClockSignalName" Type="Str">Clk40</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.MaxDutyCycle" Type="Dbl">50</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.MaxFrequency" Type="Dbl">40000000</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.MinDutyCycle" Type="Dbl">50</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.MinFrequency" Type="Dbl">40000000</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.NominalFrequency" Type="Dbl">40000000</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.PeakPeriodJitter" Type="Dbl">250</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.ResourceName" Type="Str">40 MHz Onboard Clock</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.SupportAndRequireRuntimeEnableDisable" Type="Bool">false</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.TopSignalConnect" Type="Str">Clk40</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.VariableFrequency" Type="Bool">false</Property>
					<Property Name="NI.LV.FPGA.Valid" Type="Bool">true</Property>
					<Property Name="NI.LV.FPGA.Version" Type="Int">5</Property>
				</Item>
				<Item Name="FIFO uint32" Type="FPGA FIFO">
					<Property Name="Actual Number of Elements" Type="UInt">1023</Property>
					<Property Name="Arbitration for Read" Type="UInt">1</Property>
					<Property Name="Arbitration for Write" Type="UInt">1</Property>
					<Property Name="Control Logic" Type="UInt">0</Property>
					<Property Name="Data Type" Type="UInt">7</Property>
					<Property Name="Disable on Overflow/Underflow" Type="Bool">false</Property>
					<Property Name="fifo.configuration" Type="Str">"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;FIFO uint32;DataType=1000800000000001000940070003553332000100000000000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
					<Property Name="fifo.configured" Type="Bool">true</Property>
					<Property Name="fifo.projectItemValid" Type="Bool">true</Property>
					<Property Name="fifo.valid" Type="Bool">true</Property>
					<Property Name="fifo.version" Type="Int">12</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{01BB24D9-D0FE-4251-BED1-74937723F166}</Property>
					<Property Name="Local" Type="Bool">false</Property>
					<Property Name="Memory Type" Type="UInt">2</Property>
					<Property Name="Number Of Elements Per Read" Type="UInt">1</Property>
					<Property Name="Number Of Elements Per Write" Type="UInt">1</Property>
					<Property Name="Requested Number of Elements" Type="UInt">1023</Property>
					<Property Name="Type" Type="UInt">2</Property>
					<Property Name="Type Descriptor" Type="Str">1000800000000001000940070003553332000100000000000000000000</Property>
				</Item>
				<Item Name="fpga single channel.vi" Type="VI" URL="../subVI/fpga single channel.vi">
					<Property Name="BuildSpec" Type="Str">{BAF7055D-3F8C-4A6C-9D0F-8464845CD96E}</Property>
					<Property Name="configString.guid" Type="Str">{01BB24D9-D0FE-4251-BED1-74937723F166}"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;FIFO uint32;DataType=1000800000000001000940070003553332000100000000000000000000;DisableOnOverflowUnderflow=FALSE"{03F53F69-C11C-4B73-BC97-0A75F1025C51}resource=/crio_Mod6/DIO15;0;ReadMethodType=bool;WriteMethodType=bool{05C56F73-49EB-4170-961B-F33B53BEA55F}resource=/Sleep;0;ReadMethodType=bool;WriteMethodType=bool{0914104A-EDEE-4BA9-95B7-A6DFBBA8E418}resource=/crio_Mod6/DIO25;0;ReadMethodType=bool;WriteMethodType=bool{0B497310-C9B3-47AD-A636-795C64209F44}resource=/crio_Mod5/DO10;0;ReadMethodType=bool;WriteMethodType=bool{128AE4C2-52C3-4F08-BAFD-CBF53EE925E0}resource=/crio_Mod6/DIO19;0;ReadMethodType=bool;WriteMethodType=bool{1A2DBD0E-7190-4CB2-A880-57B7A5689773}resource=/crio_Mod6/DIO27;0;ReadMethodType=bool;WriteMethodType=bool{1B56A331-12FC-45A0-A4E3-37C51C061463}resource=/crio_Mod5/DO17;0;ReadMethodType=bool;WriteMethodType=bool{22EAC336-D2C4-4D6F-AF20-E437DB291DF4}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 4,crio.Type=NI 9208,cRIOModule.AI0.DegreeRange=2,cRIOModule.AI0.TCoupleType=0,cRIOModule.AI1.DegreeRange=2,cRIOModule.AI1.TCoupleType=0,cRIOModule.AI10.DegreeRange=2,cRIOModule.AI10.TCoupleType=0,cRIOModule.AI11.DegreeRange=2,cRIOModule.AI11.TCoupleType=0,cRIOModule.AI12.DegreeRange=2,cRIOModule.AI12.TCoupleType=0,cRIOModule.AI13.DegreeRange=2,cRIOModule.AI13.TCoupleType=0,cRIOModule.AI14.DegreeRange=2,cRIOModule.AI14.TCoupleType=0,cRIOModule.AI15.DegreeRange=2,cRIOModule.AI15.TCoupleType=0,cRIOModule.AI2.DegreeRange=2,cRIOModule.AI2.TCoupleType=0,cRIOModule.AI3.DegreeRange=2,cRIOModule.AI3.TCoupleType=0,cRIOModule.AI4.DegreeRange=2,cRIOModule.AI4.TCoupleType=0,cRIOModule.AI5.DegreeRange=2,cRIOModule.AI5.TCoupleType=0,cRIOModule.AI6.DegreeRange=2,cRIOModule.AI6.TCoupleType=0,cRIOModule.AI7.DegreeRange=2,cRIOModule.AI7.TCoupleType=0,cRIOModule.AI8.DegreeRange=2,cRIOModule.AI8.TCoupleType=0,cRIOModule.AI9.DegreeRange=2,cRIOModule.AI9.TCoupleType=0,cRIOModule.Conversion Time=1,cRIOModule.Enable Open TC Detection=true,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{272078C6-9FC1-43E8-A5E9-92D79749CA53}resource=/crio_Mod5/DO15;0;ReadMethodType=bool;WriteMethodType=bool{2E6B9F3F-CEB7-4B1C-B1DC-E8BBDEBC6042}resource=/crio_Mod6/DIO7:0;0;ReadMethodType=u8;WriteMethodType=u8{2F102F6E-FDBB-40C9-8193-0BDF1D0506C7}resource=/System Reset;0;ReadMethodType=bool;WriteMethodType=bool{301132FA-CD42-4E23-BDBA-6E8B3BC49536}resource=/crio_Mod6/DIO15:8;0;ReadMethodType=u8;WriteMethodType=u8{331C7A53-9B72-4026-90B1-F621E3838009}resource=/crio_Mod5/DO31:0;0;ReadMethodType=u32;WriteMethodType=u32{35710EAD-C83B-419E-B1C2-7D141CC1D644}resource=/crio_Mod6/DIO14;0;ReadMethodType=bool;WriteMethodType=bool{35F8CE9F-BC5A-48AE-BAD2-52DE823D132D}resource=/crio_Mod6/DIO0;0;ReadMethodType=bool;WriteMethodType=bool{37D42333-D014-4484-B80B-8D29113D9B0E}resource=/crio_Mod6/DIO2;0;ReadMethodType=bool;WriteMethodType=bool{3D42045F-DE61-4EC5-9446-1302668E183D}resource=/FPGA LED;0;ReadMethodType=bool;WriteMethodType=bool{3DF8D566-6C1C-4B89-8BEB-1303BB1500CC}resource=/crio_Mod5/DO2;0;ReadMethodType=bool;WriteMethodType=bool{3FEDF4DC-2E36-450E-966D-70F56CACA015}resource=/crio_Mod6/DIO20;0;ReadMethodType=bool;WriteMethodType=bool{468A3CF1-2699-4647-8656-B5D9AB6FA27F}resource=/crio_Mod5/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8{502211C3-860E-40C3-8F96-146BAEB9042E}resource=/crio_Mod6/DIO1;0;ReadMethodType=bool;WriteMethodType=bool{53B1F9CE-A6E0-44AC-8D10-D49700B41049}resource=/Chassis Temperature;0;ReadMethodType=i16{5CEDF3E5-DC62-4E1F-BC1B-0C0AD13232AF}resource=/crio_Mod5/DO23;0;ReadMethodType=bool;WriteMethodType=bool{60A18288-CA9D-4EDE-8CA4-C0AEE259073A}resource=/crio_Mod5/DO18;0;ReadMethodType=bool;WriteMethodType=bool{6132DB0A-F9DB-4CDE-A1C4-23B42C4C4DE1}resource=/crio_Mod6/DIO22;0;ReadMethodType=bool;WriteMethodType=bool{61A861C0-FE35-45A0-8751-5582C05B485C}resource=/crio_Mod6/DIO11;0;ReadMethodType=bool;WriteMethodType=bool{6318C4D7-C920-418D-A0DF-435ED4F3E5E7}resource=/crio_Mod6/DIO4;0;ReadMethodType=bool;WriteMethodType=bool{6350A5C2-EEC0-44E8-A28C-BE72EBA13269}resource=/crio_Mod5/DO25;0;ReadMethodType=bool;WriteMethodType=bool{643C0700-2B47-45D7-B5D9-E141C5ED49C3}resource=/crio_Mod5/DO13;0;ReadMethodType=bool;WriteMethodType=bool{6442482D-7FB8-4E76-801F-DEFC530F4AC6}resource=/crio_Mod5/DO9;0;ReadMethodType=bool;WriteMethodType=bool{64F64DAE-2B98-442D-936F-0E7CC082A87F}resource=/crio_Mod6/DIO24;0;ReadMethodType=bool;WriteMethodType=bool{66103C24-4578-4E49-A423-872E667BA772}resource=/crio_Mod5/DO28;0;ReadMethodType=bool;WriteMethodType=bool{6627E99C-CF72-47E7-A944-20965D2B6EA3}resource=/crio_Mod6/DIO21;0;ReadMethodType=bool;WriteMethodType=bool{6F0C8364-A4C5-42E4-B2E0-27CD1A170F11}resource=/crio_Mod5/DO16;0;ReadMethodType=bool;WriteMethodType=bool{6FFCD3EF-128E-4462-B6BE-38A9540A307B}resource=/crio_Mod5/DO7;0;ReadMethodType=bool;WriteMethodType=bool{71D61863-399E-4C5E-9403-9C693F904EF3}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 5,crio.Type=NI 9476,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{7565E598-31EF-40D2-86EB-16CEEE2B3E8D}resource=/crio_Mod6/DIO12;0;ReadMethodType=bool;WriteMethodType=bool{79A6C91A-26D2-433D-AD65-700717963D8B}resource=/crio_Mod5/DO24;0;ReadMethodType=bool;WriteMethodType=bool{7FE88EC3-E7C5-457D-8D6A-88AAE45409F7}resource=/crio_Mod5/DO22;0;ReadMethodType=bool;WriteMethodType=bool{80B0EFCF-27E8-480A-8E8D-0232BB149B89}resource=/crio_Mod5/DO4;0;ReadMethodType=bool;WriteMethodType=bool{84E95502-F26E-4203-A62A-ED8D9215DD2A}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E{8F5A8316-30FB-4C68-8066-EB96FA55C477}resource=/crio_Mod5/DO27;0;ReadMethodType=bool;WriteMethodType=bool{93B597CC-B72C-475B-92D7-340663CF9370}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 6,crio.Type=NI 9403,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.Initial Line Direction=00000000000000000000000000000000,cRIOModule.RsiAttributes=[crioConfig.End]{93D2B685-7A60-4DD8-944F-43C66564081B}resource=/crio_Mod6/DIO26;0;ReadMethodType=bool;WriteMethodType=bool{9704869F-B668-4C38-9A20-26B00FFEDCE8}resource=/crio_Mod6/DIO28;0;ReadMethodType=bool;WriteMethodType=bool{9BDFED24-98A4-404A-9769-4EF16E7DA16A}resource=/crio_Mod5/DO6;0;ReadMethodType=bool;WriteMethodType=bool{9CCD7F23-6CFE-45A1-BE35-53F7B960D513}resource=/crio_Mod6/DIO13;0;ReadMethodType=bool;WriteMethodType=bool{A3954AAF-E743-4A06-B53B-022B82C35B89}resource=/crio_Mod5/DO8;0;ReadMethodType=bool;WriteMethodType=bool{A8392325-BEAF-4006-807D-4FB31F8C8227}resource=/crio_Mod6/DIO16;0;ReadMethodType=bool;WriteMethodType=bool{AA9BADF8-7F65-4C1D-9B87-1046E4528118}resource=/crio_Mod6/DIO5;0;ReadMethodType=bool;WriteMethodType=bool{AC14051D-D682-4466-A09F-3A34527FE7CC}resource=/crio_Mod5/DO31;0;ReadMethodType=bool;WriteMethodType=bool{ACF6E28A-50A9-4C7E-8B80-6478B56F3F52}resource=/crio_Mod6/DIO23:16;0;ReadMethodType=u8;WriteMethodType=u8{AE4095E2-F135-4177-AD13-D82352987CDD}resource=/crio_Mod6/DIO17;0;ReadMethodType=bool;WriteMethodType=bool{AE775159-FE81-4661-8C43-3EF2DFE94A31}resource=/crio_Mod6/DIO31;0;ReadMethodType=bool;WriteMethodType=bool{B0BF2D7A-5BE1-49E6-9B61-7C4052BF04CA}resource=/crio_Mod6/DIO31:0;0;ReadMethodType=u32;WriteMethodType=u32{B3EA85CF-673D-4FAA-958B-5A04A66294F1}resource=/crio_Mod5/DO19;0;ReadMethodType=bool;WriteMethodType=bool{B4117E37-6C92-44BE-9464-D86B2772B036}resource=/crio_Mod5/DO0;0;ReadMethodType=bool;WriteMethodType=bool{B9FFE86E-B964-40E5-8449-3B4AA1370004}resource=/crio_Mod5/DO20;0;ReadMethodType=bool;WriteMethodType=bool{BB589436-01B0-4A3C-B988-E80250321291}resource=/crio_Mod5/DO30;0;ReadMethodType=bool;WriteMethodType=bool{BB68FF90-E203-4D9C-8308-32DEDBF37243}resource=/crio_Mod5/DO21;0;ReadMethodType=bool;WriteMethodType=bool{BCC3F639-C4D2-42CB-AE8A-6F223391835B}resource=/crio_Mod6/DIO9;0;ReadMethodType=bool;WriteMethodType=bool{C3F2F0E3-8395-4CFB-9CAF-0FD42E696B54}resource=/crio_Mod5/DO29;0;ReadMethodType=bool;WriteMethodType=bool{C85EC4A9-8773-4357-85DF-C24E00F1B316}resource=/crio_Mod6/DIO7;0;ReadMethodType=bool;WriteMethodType=bool{C9EC5D32-2464-448F-8B83-575A7AB4FF88}resource=/crio_Mod6/DIO18;0;ReadMethodType=bool;WriteMethodType=bool{CDA2A2EC-ECCB-4DE8-BC35-71F28BD9476E}resource=/crio_Mod5/DO26;0;ReadMethodType=bool;WriteMethodType=bool{D3EFAAE5-B4B5-492E-9270-07B3F0929A68}resource=/crio_Mod5/DO31:24;0;ReadMethodType=u8;WriteMethodType=u8{D59F6A5E-350F-4D4B-B4E6-A1BA7F96DD3D}resource=/crio_Mod5/DO11;0;ReadMethodType=bool;WriteMethodType=bool{E0078FE3-2AA5-4BDA-8099-5ABCE365B6BD}resource=/crio_Mod5/DO15:8;0;ReadMethodType=u8;WriteMethodType=u8{E21EC03B-B3EB-4EB7-A8C4-DC86157F62BB}resource=/crio_Mod6/DIO6;0;ReadMethodType=bool;WriteMethodType=bool{E38FC26E-8DCE-4356-9C20-578F637AFCBB}resource=/crio_Mod6/DIO23;0;ReadMethodType=bool;WriteMethodType=bool{E4C3FD36-96B3-4C9F-9FD2-DBB47F0EEE2E}resource=/crio_Mod6/DIO29;0;ReadMethodType=bool;WriteMethodType=bool{E8AE03B9-C593-4495-AFA9-EEBCF5DDC280}resource=/crio_Mod6/DIO31:24;0;ReadMethodType=u8;WriteMethodType=u8{E8CD583D-CCEE-438A-B652-D7D3A6D1C2B9}resource=/crio_Mod6/DIO8;0;ReadMethodType=bool;WriteMethodType=bool{ECB3CA64-FD9E-41AB-8FBB-7056C486DE98}resource=/crio_Mod5/DO5;0;ReadMethodType=bool;WriteMethodType=bool{ED9A4AD4-93CA-48AB-BF36-8B208B619591}resource=/crio_Mod5/DO3;0;ReadMethodType=bool;WriteMethodType=bool{F1CF0ED8-4498-48F4-AC96-AEA85FFF7926}resource=/crio_Mod6/DIO10;0;ReadMethodType=bool;WriteMethodType=bool{F63D493A-3DF9-4FA5-BFD1-1778D7E39CBE}resource=/Scan Clock;0;ReadMethodType=bool{FAA7D784-8311-4173-9C01-32D4D0A0330E}resource=/crio_Mod5/DO1;0;ReadMethodType=bool;WriteMethodType=bool{FB614C91-BCFD-4D4E-90D0-30446CD8993B}resource=/crio_Mod6/DIO30;0;ReadMethodType=bool;WriteMethodType=bool{FBAAF3F9-C7BD-400B-B0BB-FE72E1000E54}resource=/crio_Mod5/DO14;0;ReadMethodType=bool;WriteMethodType=bool{FE9611FE-9F31-4B0B-A2E7-B675AC3530B7}resource=/crio_Mod6/DIO3;0;ReadMethodType=bool;WriteMethodType=bool{FEBA6F38-0FC7-4EF7-8694-39BFAA2581E7}resource=/crio_Mod5/DO23:16;0;ReadMethodType=u8;WriteMethodType=u8{FF90BB4B-FC8A-4B4C-920E-C3F83656BA8A}resource=/crio_Mod5/DO12;0;ReadMethodType=bool;WriteMethodType=boolcRIO-9114/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA</Property>
					<Property Name="configString.name" Type="Str">40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427EChassis Temperatureresource=/Chassis Temperature;0;ReadMethodType=i16cRIO-9114/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGAFIFO uint32"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;FIFO uint32;DataType=1000800000000001000940070003553332000100000000000000000000;DisableOnOverflowUnderflow=FALSE"FPGA LEDresource=/FPGA LED;0;ReadMethodType=bool;WriteMethodType=boolMod4[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 4,crio.Type=NI 9208,cRIOModule.AI0.DegreeRange=2,cRIOModule.AI0.TCoupleType=0,cRIOModule.AI1.DegreeRange=2,cRIOModule.AI1.TCoupleType=0,cRIOModule.AI10.DegreeRange=2,cRIOModule.AI10.TCoupleType=0,cRIOModule.AI11.DegreeRange=2,cRIOModule.AI11.TCoupleType=0,cRIOModule.AI12.DegreeRange=2,cRIOModule.AI12.TCoupleType=0,cRIOModule.AI13.DegreeRange=2,cRIOModule.AI13.TCoupleType=0,cRIOModule.AI14.DegreeRange=2,cRIOModule.AI14.TCoupleType=0,cRIOModule.AI15.DegreeRange=2,cRIOModule.AI15.TCoupleType=0,cRIOModule.AI2.DegreeRange=2,cRIOModule.AI2.TCoupleType=0,cRIOModule.AI3.DegreeRange=2,cRIOModule.AI3.TCoupleType=0,cRIOModule.AI4.DegreeRange=2,cRIOModule.AI4.TCoupleType=0,cRIOModule.AI5.DegreeRange=2,cRIOModule.AI5.TCoupleType=0,cRIOModule.AI6.DegreeRange=2,cRIOModule.AI6.TCoupleType=0,cRIOModule.AI7.DegreeRange=2,cRIOModule.AI7.TCoupleType=0,cRIOModule.AI8.DegreeRange=2,cRIOModule.AI8.TCoupleType=0,cRIOModule.AI9.DegreeRange=2,cRIOModule.AI9.TCoupleType=0,cRIOModule.Conversion Time=1,cRIOModule.Enable Open TC Detection=true,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Mod5/DO0resource=/crio_Mod5/DO0;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO10resource=/crio_Mod5/DO10;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO11resource=/crio_Mod5/DO11;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO12resource=/crio_Mod5/DO12;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO13resource=/crio_Mod5/DO13;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO14resource=/crio_Mod5/DO14;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO15:8resource=/crio_Mod5/DO15:8;0;ReadMethodType=u8;WriteMethodType=u8Mod5/DO15resource=/crio_Mod5/DO15;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO16resource=/crio_Mod5/DO16;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO17resource=/crio_Mod5/DO17;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO18resource=/crio_Mod5/DO18;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO19resource=/crio_Mod5/DO19;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO1resource=/crio_Mod5/DO1;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO20resource=/crio_Mod5/DO20;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO21resource=/crio_Mod5/DO21;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO22resource=/crio_Mod5/DO22;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO23:16resource=/crio_Mod5/DO23:16;0;ReadMethodType=u8;WriteMethodType=u8Mod5/DO23resource=/crio_Mod5/DO23;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO24resource=/crio_Mod5/DO24;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO25resource=/crio_Mod5/DO25;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO26resource=/crio_Mod5/DO26;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO27resource=/crio_Mod5/DO27;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO28resource=/crio_Mod5/DO28;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO29resource=/crio_Mod5/DO29;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO2resource=/crio_Mod5/DO2;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO30resource=/crio_Mod5/DO30;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO31:0resource=/crio_Mod5/DO31:0;0;ReadMethodType=u32;WriteMethodType=u32Mod5/DO31:24resource=/crio_Mod5/DO31:24;0;ReadMethodType=u8;WriteMethodType=u8Mod5/DO31resource=/crio_Mod5/DO31;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO3resource=/crio_Mod5/DO3;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO4resource=/crio_Mod5/DO4;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO5resource=/crio_Mod5/DO5;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO6resource=/crio_Mod5/DO6;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO7:0resource=/crio_Mod5/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8Mod5/DO7resource=/crio_Mod5/DO7;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO8resource=/crio_Mod5/DO8;0;ReadMethodType=bool;WriteMethodType=boolMod5/DO9resource=/crio_Mod5/DO9;0;ReadMethodType=bool;WriteMethodType=boolMod5[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 5,crio.Type=NI 9476,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Mod6/DIO0resource=/crio_Mod6/DIO0;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO10resource=/crio_Mod6/DIO10;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO11resource=/crio_Mod6/DIO11;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO12resource=/crio_Mod6/DIO12;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO13resource=/crio_Mod6/DIO13;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO14resource=/crio_Mod6/DIO14;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO15:8resource=/crio_Mod6/DIO15:8;0;ReadMethodType=u8;WriteMethodType=u8Mod6/DIO15resource=/crio_Mod6/DIO15;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO16resource=/crio_Mod6/DIO16;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO17resource=/crio_Mod6/DIO17;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO18resource=/crio_Mod6/DIO18;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO19resource=/crio_Mod6/DIO19;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO1resource=/crio_Mod6/DIO1;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO20resource=/crio_Mod6/DIO20;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO21resource=/crio_Mod6/DIO21;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO22resource=/crio_Mod6/DIO22;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO23:16resource=/crio_Mod6/DIO23:16;0;ReadMethodType=u8;WriteMethodType=u8Mod6/DIO23resource=/crio_Mod6/DIO23;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO24resource=/crio_Mod6/DIO24;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO25resource=/crio_Mod6/DIO25;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO26resource=/crio_Mod6/DIO26;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO27resource=/crio_Mod6/DIO27;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO28resource=/crio_Mod6/DIO28;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO29resource=/crio_Mod6/DIO29;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO2resource=/crio_Mod6/DIO2;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO30resource=/crio_Mod6/DIO30;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO31:0resource=/crio_Mod6/DIO31:0;0;ReadMethodType=u32;WriteMethodType=u32Mod6/DIO31:24resource=/crio_Mod6/DIO31:24;0;ReadMethodType=u8;WriteMethodType=u8Mod6/DIO31resource=/crio_Mod6/DIO31;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO3resource=/crio_Mod6/DIO3;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO4resource=/crio_Mod6/DIO4;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO5resource=/crio_Mod6/DIO5;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO6resource=/crio_Mod6/DIO6;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO7:0resource=/crio_Mod6/DIO7:0;0;ReadMethodType=u8;WriteMethodType=u8Mod6/DIO7resource=/crio_Mod6/DIO7;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO8resource=/crio_Mod6/DIO8;0;ReadMethodType=bool;WriteMethodType=boolMod6/DIO9resource=/crio_Mod6/DIO9;0;ReadMethodType=bool;WriteMethodType=boolMod6[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 6,crio.Type=NI 9403,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.Initial Line Direction=00000000000000000000000000000000,cRIOModule.RsiAttributes=[crioConfig.End]Scan Clockresource=/Scan Clock;0;ReadMethodType=boolSleepresource=/Sleep;0;ReadMethodType=bool;WriteMethodType=boolSystem Resetresource=/System Reset;0;ReadMethodType=bool;WriteMethodType=bool</Property>
					<Property Name="NI.LV.FPGA.InterfaceBitfile" Type="Str">W:\Engineering\Current Projects\Civil &amp; Arch Eng\Brant\Pervaporation\field system\software\current\FPGA Bitfiles\satish_FPGATarget_fpgamultchan_F2nsrnTgWBY.lvbitx</Property>
				</Item>
				<Item Name="Mod5" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Slot 5</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">false</Property>
					<Property Name="crio.Type" Type="Str">NI 9476</Property>
					<Property Name="cRIOModule.DisableArbitration" Type="Str">false</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{71D61863-399E-4C5E-9403-9C693F904EF3}</Property>
				</Item>
				<Item Name="Mod6" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Slot 6</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">true</Property>
					<Property Name="crio.Type" Type="Str">NI 9403</Property>
					<Property Name="cRIOModule.DisableArbitration" Type="Str">false</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="cRIOModule.Initial Line Direction" Type="Str">00000000000000000000000000000000</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{93B597CC-B72C-475B-92D7-340663CF9370}</Property>
				</Item>
				<Item Name="Mod4" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Slot 4</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">false</Property>
					<Property Name="crio.Type" Type="Str">NI 9208</Property>
					<Property Name="cRIOModule.AI0.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI0.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI1.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI1.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI10.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI10.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI11.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI11.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI12.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI12.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI13.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI13.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI14.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI14.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI15.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI15.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI2.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI2.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI3.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI3.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI4.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI4.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI5.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI5.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI6.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI6.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI7.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI7.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI8.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI8.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI9.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI9.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.Conversion Time" Type="Str">1</Property>
					<Property Name="cRIOModule.Enable Open TC Detection" Type="Str">true</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{22EAC336-D2C4-4D6F-AF20-E437DB291DF4}</Property>
				</Item>
				<Item Name="Dependencies" Type="Dependencies">
					<Item Name="vi.lib" Type="Folder">
						<Item Name="niFPGA Boolean Crossing.vi" Type="VI" URL="/&lt;vilib&gt;/express/rvi/analysis/control/nonlinear/niFPGA Boolean Crossing.vi"/>
					</Item>
					<Item Name="niFpgaSctlEmulationGetInTimedLoop.vi" Type="VI" URL="/&lt;vilib&gt;/rvi/Emulation/niFpgaSctlEmulationGetInTimedLoop.vi"/>
					<Item Name="niFpgaSetErrorForExecOnDevCompSimple.vi" Type="VI" URL="/&lt;vilib&gt;/rvi/errors/niFpgaSetErrorForExecOnDevCompSimple.vi"/>
					<Item Name="niFpgaGetScratchAppInstance.vi" Type="VI" URL="/&lt;vilib&gt;/rvi/eio/common/niFpgaGetScratchAppInstance.vi"/>
					<Item Name="nirviEmuReportErrorAndStop.vi" Type="VI" URL="/&lt;vilib&gt;/rvi/eio/common/nirviEmuReportErrorAndStop.vi"/>
					<Item Name="niFpgaSctlEmulationClkInfo.ctl" Type="VI" URL="/&lt;vilib&gt;/rvi/Emulation/niFpgaSctlEmulationClkInfo.ctl"/>
					<Item Name="niFpgaSctlEmulationSchedulerRegClks.vi" Type="VI" URL="/&lt;vilib&gt;/rvi/Emulation/niFpgaSctlEmulationSchedulerRegClks.vi"/>
					<Item Name="nirviTagForDefaultClock.vi" Type="VI" URL="/&lt;vilib&gt;/rvi/ClientSDK/Core/TimingSources/Configuration/Public/nirviTagForDefaultClock.vi"/>
					<Item Name="niFpgaSctlEmulationConstants.vi" Type="VI" URL="/&lt;vilib&gt;/rvi/Emulation/niFpgaSctlEmulationConstants.vi"/>
					<Item Name="niFpgaGenCallStack.vi" Type="VI" URL="/&lt;vilib&gt;/rvi/errors/niFpgaGenCallStack.vi"/>
					<Item Name="nirviFillInErrorInfo.vi" Type="VI" URL="/&lt;vilib&gt;/rvi/errors/nirviFillInErrorInfo.vi"/>
					<Item Name="niFpgaSctlEmulationIdMgrCmd.ctl" Type="VI" URL="/&lt;vilib&gt;/rvi/Emulation/niFpgaSctlEmulationIdMgrCmd.ctl"/>
					<Item Name="niFpgaSctlEmulationIdMgr.vi" Type="VI" URL="/&lt;vilib&gt;/rvi/Emulation/niFpgaSctlEmulationIdMgr.vi"/>
					<Item Name="niFpgaSctlEmulationSchedulerHandleRollover.vi" Type="VI" URL="/&lt;vilib&gt;/rvi/Emulation/niFpgaSctlEmulationSchedulerHandleRollover.vi"/>
					<Item Name="nirvimemoryEmulationManagerCacheLock_Operations.ctl" Type="VI" URL="/&lt;vilib&gt;/rvi/Memory/Memory_Emulation/nirvimemoryEmulationManagerCacheLock_Operations.ctl"/>
					<Item Name="nirvimemoryEmulationManagerCacheLock.vi" Type="VI" URL="/&lt;vilib&gt;/rvi/Memory/Memory_Emulation/nirvimemoryEmulationManagerCacheLock.vi"/>
					<Item Name="nirvimemoryEmulationManagerCache_ReleaseExclusive.vi" Type="VI" URL="/&lt;vilib&gt;/rvi/Memory/Memory_Emulation/nirvimemoryEmulationManagerCache_ReleaseExclusive.vi"/>
					<Item Name="nirvimemoryEmulationManagerCache_Operations.ctl" Type="VI" URL="/&lt;vilib&gt;/rvi/Memory/Memory_Emulation/nirvimemoryEmulationManagerCache_Operations.ctl"/>
					<Item Name="nirvimemoryEmulationManagerCache.vi" Type="VI" URL="/&lt;vilib&gt;/rvi/Memory/Memory_Emulation/nirvimemoryEmulationManagerCache.vi"/>
					<Item Name="nirvimemoryEmulationManagerCache_GetValue.vi" Type="VI" URL="/&lt;vilib&gt;/rvi/Memory/Memory_Emulation/nirvimemoryEmulationManagerCache_GetValue.vi"/>
					<Item Name="nirvimemoryEmulationManagerCache_MakeExclusive.vi" Type="VI" URL="/&lt;vilib&gt;/rvi/Memory/Memory_Emulation/nirvimemoryEmulationManagerCache_MakeExclusive.vi"/>
					<Item Name="nirvimemoryEmulationManagerCache_SetValue.vi" Type="VI" URL="/&lt;vilib&gt;/rvi/Memory/Memory_Emulation/nirvimemoryEmulationManagerCache_SetValue.vi"/>
					<Item Name="niFpgaSctlEmulationFifoFullMgr.vi" Type="VI" URL="/&lt;vilib&gt;/rvi/Emulation/niFpgaSctlEmulationFifoFullMgr.vi"/>
					<Item Name="niFpgaSctlEmulationSharedResTypes.ctl" Type="VI" URL="/&lt;vilib&gt;/rvi/Emulation/niFpgaSctlEmulationSharedResTypes.ctl"/>
					<Item Name="niFpgaSctlEmulationSharedResource.ctl" Type="VI" URL="/&lt;vilib&gt;/rvi/Emulation/niFpgaSctlEmulationSharedResource.ctl"/>
					<Item Name="niFpgaSctlEmulationSharedResMgrCmd.ctl" Type="VI" URL="/&lt;vilib&gt;/rvi/Emulation/niFpgaSctlEmulationSharedResMgrCmd.ctl"/>
					<Item Name="niFpgaSctlEmulationResourceMgr.vi" Type="VI" URL="/&lt;vilib&gt;/rvi/Emulation/niFpgaSctlEmulationResourceMgr.vi"/>
					<Item Name="nirviReportUnexpectedCaseInternalErrorHelper.vi" Type="VI" URL="/&lt;vilib&gt;/rvi/errors/nirviReportUnexpectedCaseInternalErrorHelper.vi"/>
					<Item Name="nirviReportUnexpectedCaseInternalError (String).vi" Type="VI" URL="/&lt;vilib&gt;/rvi/errors/nirviReportUnexpectedCaseInternalError (String).vi"/>
					<Item Name="niFpgaSctlEmulationSchedulerUnRegClks.vi" Type="VI" URL="/&lt;vilib&gt;/rvi/Emulation/niFpgaSctlEmulationSchedulerUnRegClks.vi"/>
					<Item Name="niFpgaSctlEmulationSchedulerGenSchedule.vi" Type="VI" URL="/&lt;vilib&gt;/rvi/Emulation/niFpgaSctlEmulationSchedulerGenSchedule.vi"/>
					<Item Name="niFpgaSctlEmulationSchedulerState.ctl" Type="VI" URL="/&lt;vilib&gt;/rvi/Emulation/niFpgaSctlEmulationSchedulerState.ctl"/>
					<Item Name="niFpgaSctlEmulationSchedulerCommand.ctl" Type="VI" URL="/&lt;vilib&gt;/rvi/Emulation/niFpgaSctlEmulationSchedulerCommand.ctl"/>
					<Item Name="niFpgaSctlEmulationScheduler.vi" Type="VI" URL="/&lt;vilib&gt;/rvi/Emulation/niFpgaSctlEmulationScheduler.vi"/>
					<Item Name="niFpgaSctlEmulationGlobalWrite.vi" Type="VI" URL="/&lt;vilib&gt;/rvi/Emulation/niFpgaSctlEmulationGlobalWrite.vi"/>
					<Item Name="niFpgaSctlEmulationRegisterWithScheduler.vi" Type="VI" URL="/&lt;vilib&gt;/rvi/Emulation/niFpgaSctlEmulationRegisterWithScheduler.vi"/>
					<Item Name="niFpgaEmulationVisToLoad.vi" Type="VI" URL="/&lt;vilib&gt;/rvi/Emulation/niFpgaEmulationVisToLoad.vi"/>
				</Item>
				<Item Name="Build Specifications" Type="Build">
					<Item Name="fpga single chan" Type="{F4C5E96F-7410-48A5-BB87-3559BC9B167F}">
						<Property Name="AllowEnableRemoval" Type="Bool">false</Property>
						<Property Name="BuildSpecDecription" Type="Str"></Property>
						<Property Name="BuildSpecName" Type="Str">fpga single chan</Property>
						<Property Name="Comp.BitfileName" Type="Str">satish_FPGATarget_fpgasinglechan_nWA0kfWPjuM.lvbitx</Property>
						<Property Name="Comp.CustomXilinxParameters" Type="Str"></Property>
						<Property Name="Comp.MaxFanout" Type="Int">-1</Property>
						<Property Name="Comp.RandomSeed" Type="Bool">false</Property>
						<Property Name="Comp.Version.Build" Type="Int">0</Property>
						<Property Name="Comp.Version.Fix" Type="Int">0</Property>
						<Property Name="Comp.Version.Major" Type="Int">1</Property>
						<Property Name="Comp.Version.Minor" Type="Int">0</Property>
						<Property Name="Comp.VersionAutoIncrement" Type="Bool">false</Property>
						<Property Name="Comp.Xilinx.DesignStrategy" Type="Str">balanced</Property>
						<Property Name="Comp.Xilinx.MapEffort" Type="Str">high(timing)</Property>
						<Property Name="Comp.Xilinx.ParEffort" Type="Str">standard</Property>
						<Property Name="Comp.Xilinx.SynthEffort" Type="Str">normal</Property>
						<Property Name="Comp.Xilinx.SynthGoal" Type="Str">speed</Property>
						<Property Name="Comp.Xilinx.UseRecommended" Type="Bool">true</Property>
						<Property Name="DefaultBuildSpec" Type="Bool">true</Property>
						<Property Name="DestinationDirectory" Type="Path">FPGA Bitfiles</Property>
						<Property Name="ProjectPath" Type="Path">/V/Engineering/Code snippets/Decagon 5TE/satish.lvproj</Property>
						<Property Name="RelativePath" Type="Bool">true</Property>
						<Property Name="RunWhenLoaded" Type="Bool">false</Property>
						<Property Name="SupportDownload" Type="Bool">true</Property>
						<Property Name="SupportResourceEstimation" Type="Bool">true</Property>
						<Property Name="TargetName" Type="Str">FPGA Target</Property>
						<Property Name="TopLevelVI" Type="Ref">/pervap-sheridan/Chassis/FPGA Target/fpga single channel.vi</Property>
					</Item>
				</Item>
			</Item>
		</Item>
		<Item Name="RT simple.vi" Type="VI" URL="../RT simple.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="NI_AALPro.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALPro.lvlib"/>
				<Item Name="niFPGA Boolean Crossing.vi" Type="VI" URL="/&lt;vilib&gt;/express/rvi/analysis/control/nonlinear/niFPGA Boolean Crossing.vi"/>
			</Item>
			<Item Name="NiFpgaLv.dll" Type="Document" URL="NiFpgaLv.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
